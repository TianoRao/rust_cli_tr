# Black Scholes Calculator using Rust CLI
Tianji Rao

## Introduction

This Rust command-line tool calculates the call option price using the Black Scholes model. Designed for both educational purposes and practical use, it highlights the power of Rust for financial computing. The author also build a CICD pipeline for this project.

## Features

- **Fast and Efficient**: Leveraging Rust's performance to ensure quick calculations.
- **User-Friendly CLI**: Easy-to-navigate command-line interface with helpful prompts.
- **Precise Calculations**: Utilizes the `statrs` library for accurate statistical operations.

## Data Preprocessing
Before running the Black Scholes Calculator, it's essential to ensure that input data is correctly preprocessed to maintain accuracy in the option pricing calculation. This involves validating that all input parameters—stock price (`s0`), strike price (`x`), risk-free interest rate (`r`), volatility (`sigma`), and time to expiration (`t`)—are numeric and within reasonable ranges. Special attention should be given to the `sigma` and `t` parameters to prevent negative values, which are not permissible within the Black Scholes model framework. Proper data preprocessing not only guards against computational errors but also guarantees that the results are based on realistic and reliable financial assumptions.


## Getting Started

### Prerequisites

- Rust and Cargo installed on your system. If you haven't installed Rust yet, follow the instructions [here](https://www.rust-lang.org/tools/install).

### Installation

1. Clone the repository:
   ```bash
   git clone <repository-url>
    ```
2. Navigate to the project directory:
    ```bash
    cd rust_cli
    ```
3. Build the project:
    ```bash
    cargo build --release
    ```

## Usage
To run the tool, use the following command from the project directory:
```bash
cargo run -- -s0 <stock price> -x <strike price> -r <risk-free rate> -sigma <volatility> -t <time to expiration>
```

### Sample Input and Output
```bash
cargo run -- -s0 100 -x 100 -r 0.05 -sigma 0.2 -t 1
```

Sample outcome:
```bash
Call Option Price: 10.261595741546323
```

![Sample run](./fig/test.png)


## Testing
To ensure the reliability and accuracy of the calculations, comprehensive unit tests have been included.

Run the tests using the following command:
```bash
cargo test
```

### Testing Report

![Testing report](./fig/run.png)


### References
- https://github.com/alfredodeza/rust-cli-example
- https://www.rust-lang.org/what/cli
- https://docs.rs/clap/latest/clap/
