use clap::ColorChoice;
use clap::{Arg, Command};
use serde::Serialize;
use statrs::distribution::{ContinuousCDF, Normal};
use std::f64::consts::E;

#[derive(Serialize)]
struct OptionParams {
    s: f64,
    x: f64,
    r: f64,
    sigma: f64,
    t: f64,
}

fn black_scholes_call_price(s: f64, x: f64, r: f64, sigma: f64, t: f64) -> f64 {
    let d1 = (s.ln() / x + (r + sigma.powi(2) / 2.0) * t) / (sigma * t.sqrt());
    let d2 = d1 - sigma * t.sqrt();
    let norm_dist = Normal::new(0.0, 1.0).unwrap();
    let nd1 = norm_dist.cdf(d1);
    let nd2 = norm_dist.cdf(d2);

    s * nd1 - x * E.powf(-r * t) * nd2
}

fn main() {
    let matches = Command::new("Black Scholes Call")
        .version("1.0")
        .author("T.T.Rao")
        .about("Calculates the call option price using the Black Scholes model")
        .color(ColorChoice::Always)
        .arg(
            Arg::new("s")
                .help("Current stock price")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::new("x")
                .help("Strike price")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::new("r")
                .help("Risk-free interest rate")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::new("sigma")
                .help("Volatility")
                .required(true)
                .takes_value(true),
        )
        .arg(
            Arg::new("t")
                .help("Time to expiration in years")
                .required(true)
                .takes_value(true),
        )
        .get_matches();

    let s = matches.value_of("s").unwrap().parse::<f64>().unwrap();
    let x = matches.value_of("x").unwrap().parse::<f64>().unwrap();
    let r = matches.value_of("r").unwrap().parse::<f64>().unwrap();
    let sigma = matches.value_of("sigma").unwrap().parse::<f64>().unwrap();
    let t = matches.value_of("t").unwrap().parse::<f64>().unwrap();

    let call_price = black_scholes_call_price(s, x, r, sigma, t);

    println!("Call Option Price: {}", call_price);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_black_scholes_call_price() {
        let call_price = black_scholes_call_price(100.0, 100.0, 0.05, 0.2, 1.0);
        let expected_price = 10.261595741546323; 
        assert!((call_price - expected_price).abs() < 1e-5);
    }
}
